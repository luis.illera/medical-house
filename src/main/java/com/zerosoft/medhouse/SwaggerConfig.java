package com.zerosoft.medhouse;

import com.zerosoft.medhouse.configuration.model.AppProperties;
import com.zerosoft.medhouse.configuration.model.License;
import com.zerosoft.medhouse.configuration.model.SwaggerApiInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Autowired
    protected AppProperties appProperties;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(appProperties.getSwagger().getApiInfo().getBasePackage()))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        final SwaggerApiInfo appApiInfo = appProperties.getSwagger().getApiInfo();
        final Contact contact = appProperties.getSwagger().getContact();
        final License license = appProperties.getSwagger().getLicense();
        return new ApiInfo(appApiInfo.getTitle(), appApiInfo.getDescription(), appApiInfo.getVersion(),
                appApiInfo.getTosUrl(), contact, license.getName(), license.getUrl(),
                Collections.emptyList());
    }

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(Arrays.asList("*"));
        config.setAllowedMethods(Arrays.asList("*"));
        config.setAllowedHeaders(Arrays.asList("*"));
        config.setExposedHeaders(Arrays.asList("ERROR"));
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

    @Bean
    public MessageSourceAccessor messageSourceAccessor(MessageSource messageSource) {
        return new MessageSourceAccessor(messageSource, Locale.getDefault());
    }

}