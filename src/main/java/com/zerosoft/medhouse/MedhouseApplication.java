package com.zerosoft.medhouse;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedhouseApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(MedhouseApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }
}
