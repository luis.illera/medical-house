package com.zerosoft.medhouse.configuration.model;

@lombok.Getter
public class Swagger {
    private final SwaggerApiInfo apiInfo;
    private final springfox.documentation.service.Contact contact;
    private final License license;

    public Swagger(final SwaggerApiInfo apiInfo, final springfox.documentation.service.Contact contact,
                   final License license) {
        this.apiInfo = apiInfo;
        this.contact = contact;
        this.license = license;
    }
}
