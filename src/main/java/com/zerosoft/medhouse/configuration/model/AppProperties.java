package com.zerosoft.medhouse.configuration.model;

@lombok.Getter
public class AppProperties {

    private final Swagger swagger;

    public AppProperties(final Swagger swagger) {
        this.swagger = swagger;
    }
}
