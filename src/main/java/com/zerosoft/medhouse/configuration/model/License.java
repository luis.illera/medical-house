package com.zerosoft.medhouse.configuration.model;

@lombok.Getter
public class License {

    private final String name;
    private final String url;

    public License(final String name, final String url) {
        this.name = name;
        this.url = url;
    }
}
