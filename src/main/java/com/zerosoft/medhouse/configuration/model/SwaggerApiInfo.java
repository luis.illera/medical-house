package com.zerosoft.medhouse.configuration.model;

@lombok.Getter
public class SwaggerApiInfo {
    private final String version;
    private final String basePackage;
    private final String title;
    private final String description;
    private final String tosUrl;

    public SwaggerApiInfo(final String version, final String basePackage, final String title, final String description, final String tosUrl) {
        this.version = version;
        this.basePackage = basePackage;
        this.title = title;
        this.description = description;
        this.tosUrl = tosUrl;
    }
}