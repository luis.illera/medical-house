package com.zerosoft.medhouse.configuration;

import com.zerosoft.medhouse.configuration.model.*;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.Contact;

@Configuration
public class BeansConfiguration {

    @Bean
    public AppProperties appProperties(
            @Value("${app.swagger.apiInfo.version}") final String version,
            @Value("${app.swagger.apiInfo.base-package}") final String basePackage,
            @Value("${app.swagger.apiInfo.title}") final String title,
            @Value("${app.swagger.apiInfo.description}") final String description,
            @Value("${app.swagger.apiInfo.terms-of-service-url}") final String tosUrl,
            @Value("${app.swagger.contact.name}") final String contactName,
            @Value("${app.swagger.contact.email}") final String contactEmail,
            @Value("${app.swagger.license.name}") final String licenseName,
            @Value("${app.swagger.license.url}") final String licenseUrl) {
        final SwaggerApiInfo swaggerApiInfo = new SwaggerApiInfo(version, basePackage, title, description, tosUrl);
        final Contact contact = new Contact(contactName, Strings.EMPTY, contactEmail);
        final License license = new License(licenseName, licenseUrl);
        final Swagger swagger = new Swagger(swaggerApiInfo, contact, license);
        final AppProperties appProperties = new AppProperties(swagger);
        return appProperties;
    }
}
